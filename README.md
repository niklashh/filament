# Filament?

## Send to OctoPrint using curl

```shell
curl -H 'X-Api-Key: YOUR_API_KEY' -v -F print=true -F path= -F 'file=@compiled.gcode' octoprint:port/api/files/local
```
