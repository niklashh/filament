use std::f32::consts::PI;
use std::io::prelude::*;

#[derive(Copy, Clone, Debug)]
struct Point(f32, f32);

impl std::ops::Add<Point> for Point {
    type Output = Point;
    fn add(self, rhs: Point) -> Point {
        Point(self.0 + rhs.0, self.1 + rhs.1)
    }
}

const EXTRUSION_PER_MM: f32 = 0.283f32;

fn gcode(points: &Vec<Point>, z: f32, feedrate: f32, extrusion_mult: f32) -> String {
    assert!(points.len() > 1, "must contain at least two points");
    let mut prev_point = points.get(0).unwrap();
    let mut result = String::from(&format!(
        "G1 E-4 F{}\nG1 Z{}\nG1 X{} Y{}\nG1 Z{} E4\nG1 F{}",
        feedrate * 2f32,
        z + 2f32,
        prev_point.0,
        prev_point.1,
        z,
        feedrate,
    ));
    for point in points.as_slice()[1..].iter() {
        let hypot = (point.0 - prev_point.0).hypot(point.1 - prev_point.1);
        prev_point = &point;
        result.push_str(&format!(
            "\nG1 X{} Y{} E{}",
            point.0,
            point.1,
            hypot * EXTRUSION_PER_MM * extrusion_mult
        ));
    }
    result
}

// fn spiral(
//     center: Point,
//     radius: f32,
//     growth: f32,
//     startangle: f32,
//     endangle: f32,
//     detail: f32,
// ) -> Vec<Point> {
//     ((startangle * detail) as u32..(endangle * detail) as u32)
//         .into_iter()
//         .map(|a| {
//             let a = a as f32 / detail;
//             center
//                 + Point(
//                     (radius + a / (2f32 * PI) * growth) * a.cos(),
//                     (radius + a / (2f32 * PI) * growth) * a.sin(),
//                 )
//         })
//         .collect::<Vec<Point>>()
// }

enum End {
    Head,
    Tail,
}

struct ColorPart((f32, End), (f32, End));

const DETAIL: f32 = 72f32;

fn parts(
    center: Point,
    radius: f32,
    growth: f32,
    color_parts: &[ColorPart],
) -> Vec<Vec<Vec<Point>>> {
    color_parts
        .iter()
        .map(|color_part| {
            let start = &color_part.0;
            let end = &color_part.1;

            // make end 0.01rad longer

            let layer1 = ((start.0 * DETAIL) as u32..((end.0 + 0.01f32 * PI) * DETAIL) as u32)
                .into_iter()
                .map(|a| {
                    let a = a as f32 / DETAIL;
                    center
                        + Point(
                            (radius + a / (2f32 * PI) * growth) * a.cos(),
                            (radius + a / (2f32 * PI) * growth) * a.sin(),
                        )
                })
                .collect::<Vec<Point>>();

            let startangle = start.0
                + match start.1 {
                    End::Head => 1f32,
                    End::Tail => -1f32,
                } * 0.05f32
                    * PI;
            let endangle = end.0
                + match end.1 {
                    End::Head => -1f32,
                    End::Tail => 1f32,
                } * 0.05f32
                    * PI;
            let layer2 = ((startangle * DETAIL) as u32
                ..((endangle + 0.01f32 * PI) * DETAIL) as u32)
                .into_iter()
                .map(|a| {
                    let a = a as f32 / DETAIL;
                    center
                        + Point(
                            (radius + a / (2f32 * PI) * growth) * a.cos(),
                            (radius + a / (2f32 * PI) * growth) * a.sin(),
                        )
                })
                .collect::<Vec<Point>>();

            vec![layer1, layer2]
        })
        .collect::<Vec<Vec<Vec<Point>>>>()
}

fn parts_to_gcode(parts: &Vec<Vec<Vec<Point>>>) -> String {
    parts
        .iter()
        .map(|part| {
            let layer1 = part.get(0).unwrap();
            let mut g = gcode(layer1, 0.3f32, 700f32, 0.5f32);
            let layer2 = part.get(1).unwrap();
            g.push_str(&gcode(layer2, 1.0f32, 1200f32, 1.3f32));
            //    let layer3 = parts.get(2).unwrap();
            //    g.push_str(&gcode(layer3, 1.3f32, 1200f32, 1f32));
            g
        })
        .collect::<Vec<String>>()
        .join(&"\nM600\n")
}

fn main() {
    let cp = vec![
        ColorPart((1.5f32 * PI, End::Head), (2.5f32 * PI, End::Head)),
        ColorPart((2.5f32 * PI, End::Tail), (3.5f32 * PI, End::Head)),
    ];
    let p = parts(Point(100f32, 100f32), 50f32, 4f32, &cp[..]);
    let g = parts_to_gcode(&p);

    //    let g = format!(
    //        "{}\n{}\n{}\nM600\n{}\n{}\n{}\nG1 E-4",
    //        g1, g2, g3, g4, g5, g6
    //    );
    //    let g = format!("{}\n{}\nM600\n{}\nG1 E-4", g1, g2, g3);

    let mut fd = std::fs::File::create(std::path::Path::new("./output.gcode")).unwrap();
    fd.write_all(&g.as_bytes()).unwrap();
}
