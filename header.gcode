M73 P0 R2
M201 X9000 Y9000 Z500 E10000 ; sets maximum accelerations, mm/sec^2
M203 X500 Y500 Z12 E120 ; sets maximum feedrates, mm/sec
M204 P2000 R1500 T2000 ; sets acceleration (P, T) and retract acceleration (R), mm/sec^2
M205 X10.00 Y10.00 Z0.20 E2.50 ; sets the jerk limits, mm/sec
M205 S0 T0 ; sets the minimum extruding and travel feed rate, mm/sec
M107
;TYPE:Custom
M862.3 P "MK2S" ; printer model check
M862.1 P1 ; nozzle diameter check
M115 U3.2.3 ; tell printer latest fw version
G90 ; use absolute coordinates
M83 ; extruder relative mode
M204 S2000 T1500 ; MK2 firmware only supports the old M204 format
M104 S215 ; set extruder temp
M140 S60 ; set bed temp
M190 S60 ; wait for bed temp
M109 S215 ; wait for extruder temp
G28 W ; home all without mesh bed level
G80 ; mesh bed leveling
G1 Y-3.0 F1000.0 ; go outside print area
G92 E0.0
M73 P4 R2
G1 X60.0 E9.0  F1000.0 ; intro line
M73 P6 R2
G1 X100.0 E12.5 F1000.0 ; intro line
G92 E0.0
G21 ; set units to millimeters
G90 ; use absolute coordinates
M83 ; use relative distances for extrusion
M900 K30 ; Filament gcode
;Z:0.5
;HEIGHT:0.5
;BEFORE_LAYER_CHANGE
G92 E0.0
;0.5


G1 E-0.80000 F2100.00000
G1 Z0.600 F10800.000
